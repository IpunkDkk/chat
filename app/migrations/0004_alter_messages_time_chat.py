# Generated by Django 3.2.6 on 2021-08-21 10:57

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_alter_messages_time_chat'),
    ]

    operations = [
        migrations.AlterField(
            model_name='messages',
            name='time_chat',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
    ]
