from rest_framework import serializers
from django.contrib.auth.models import User
from app.models import Messages , Room ,UserProfile



class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ["id_user" , "nama_lengkap" , "tanggal_lahir" , "alamat" , "nomerhp"]


class RoomSerializers(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['id' , 'detail']


# MessagesSerializers
class MessagesSerializers(serializers.ModelSerializer):
    class Meta:
        model = Messages
        fields = ['id_send' , 'id_room' , 'messages' , 'time_chat' ]



# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

        return user

