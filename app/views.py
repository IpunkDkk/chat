from django.db.models import query
from rest_framework import generics, permissions, views , viewsets , status
from rest_framework.response import Response
from knox.models import AuthToken
from django.contrib.auth.models import User
from app.serializer import UserProfileSerializer, UserSerializer, RegisterSerializer , MessagesSerializers, RoomSerializers
from django.contrib.auth import login
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView
from app.models import Messages , Room , UserProfile

class UserProfileView(generics.GenericAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    def post(self , request, *args , **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status.HTTP_200_OK)
    def get(self, request):
        query = UserProfile.objects.all()
        serializer = UserProfileSerializer(query , many = True)
        return Response(serializer.data ,status=status.HTTP_404_NOT_FOUND)

class GetProfile(generics.GenericAPIView):
    def get(self, request, id):
        try : 
            query = UserProfile.objects.get(id_user = id)
            serializer = UserProfileSerializer(query , many=False)
            return Response(serializer.data)
        except :
            query = UserProfile.objects.all()
            serializer = UserProfileSerializer(query , many = True)
            return Response(serializer.data ,status=status.HTTP_404_NOT_FOUND)

class MessagesView(generics.GenericAPIView):
    queryset = Messages.objects.all()
    serializer_class = MessagesSerializers
    def post(self, request, *args , **kwargs):
        serializer = MessagesSerializers(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer = serializer.save()
        return Response(status=status.HTTP_201_CREATED)
    @staticmethod
    def get(self):
        query = Messages.objects.all()
        serializer = MessagesSerializers(query , many = True)
        return Response(serializer.data)

class GetMessage(generics.GenericAPIView):
    queryset = Messages.objects.all()
    serializer = MessagesSerializers
    def get(self , request, id):
        query = Messages.objects.filter(id_room = id)
        serializer = MessagesSerializers(query , many = True)
        return Response(serializer.data)

class RoomSet(generics.GenericAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializers
    def post(self , request, *args , **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
    def get(self, request):
        query = Room.objects.all()
        serializer = RoomSerializers(query , many = True)
        return Response(serializer.data)
    def delete(self, request , id):
        try :
            room = Room.objects.get(id=id)
            room.delete()
            return Response(status=status.HTTP_200_OK)
        except :
            return Response(status=status.HTTP_404_NOT_FOUND)

# user views
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
#     # permission_classes = [permissions.IsAuthenticated]
# class UserViewSet(generics.GenericAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#     def get(self, request):
#         query = User.objects.all()
#         serializer = UserSerializer(query , many = True)
#         return Response(serializer.data)

# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
        "user": UserSerializer(user, context=self.get_serializer_context()).data,
        "token": AuthToken.objects.create(user)[1]
        })

class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)