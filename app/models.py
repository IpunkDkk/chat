from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

class Room(models.Model):
    detail = models.CharField(max_length=200 , default=None)
    # id_receive = models.ForeignKey(User , default=None, on_delete=models.CASCADE, related_name="penerima")


class Messages(models.Model):
    id_send = models.ForeignKey(User , default=None, on_delete=models.CASCADE, related_name="pengirim" , null=False)
    id_room = models.ForeignKey(Room ,default=None, on_delete=models.CASCADE , related_name="Id_Room" , null=False)
    messages = models.CharField(max_length=1000)
    time_chat = models.DateTimeField(auto_now_add=True, null=True)

class UserProfile(models.Model):
    id_user = models.ForeignKey(User, default=None , on_delete=models.CASCADE, related_name="profile" ) 
    nama_lengkap = models.CharField(max_length=200)
    tanggal_lahir = models.CharField(max_length=50)
    alamat = models.CharField(max_length=200)
    nomerhp = models.CharField(max_length=200)