from django.urls.conf import include
from app.views import GetMessage, RegisterAPI , LoginAPI, UserProfileView , UserViewSet, MessagesView , RoomSet
from django.urls import path , include
from knox import views as knox_views
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    # path('api/users/', UserViewSet.as_view() , name='Users'),
    path('api/profile/', UserProfileView.as_view() , name='profile'),
    path('api/rooms/', RoomSet.as_view() , name='rooms'),
    path('api/rooms/<int:id>', RoomSet.as_view() , name='rooms'),
    path('api/messages/', MessagesView.as_view() , name='messages'),
    path('api/message/<int:id>', GetMessage.as_view() , name='message'),
    path('api/register/', RegisterAPI.as_view(), name='register'),
    path('api/login/', LoginAPI.as_view(), name='login'),
    path('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('api/logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall'),
]

